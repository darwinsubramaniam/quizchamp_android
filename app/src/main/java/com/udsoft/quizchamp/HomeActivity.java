package com.udsoft.quizchamp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.label_user_name)
    TextView labelUserName;
    @BindView(R.id.label_user_level)
    TextView labelUserLevel;
    @BindView(R.id.label_user_correct)
    TextView labelUserCorrect;
    @BindView(R.id.label_user_wrong)
    TextView labelUserWrong;
    @BindView(R.id.user_details_frame)
    FrameLayout userDetailsFrame;
    @BindView(R.id.user_profile_Image)
    ImageView userProfileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.user_details_frame)
    public void onUserDetailsFrameClicked() {
    }

    @OnClick(R.id.user_profile_Image)
    public void onUserProfileImageClicked() {
    }
}
