package com.udsoft.quizchamp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.strongloop.android.loopback.AccessToken;
import com.strongloop.android.loopback.RestAdapter;
import com.udsoft.quizchamp.UDSoft.BackgroundActivityCaller;
import com.udsoft.quizchamp.UDSoft.Master;
import com.udsoft.quizchamp.Server.Server;
import com.udsoft.quizchamp.Models.User.UserModel;
import com.udsoft.quizchamp.Models.User.UserRepository;
import com.udsoft.quizchamp.Server.Services.ProfilePullService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    public static final String USER_INFO = "UserInfo";


    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.bt_signin)
    Button btSignin;
    @BindView(R.id.link_register)
    TextView linkRegister;

    private RestAdapter adapter;

    private String userEmail;
    private String userPassword;
    public String assignedToken;
    private String firstname;
    private String lastname;
    private String dob;
    private String ID;
    private boolean registerNewUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        adapter = new Server().getLoopbackAdapter(getApplicationContext());
    }

    private boolean validateInput() {
        boolean isValid = false;
        boolean isPasswordValid;

        String tempEmail = inputEmail.getText().toString();

        boolean isEmailValid = Patterns.EMAIL_ADDRESS.matcher(tempEmail).matches();

        if (!isEmailValid) {
            inputEmail.setError("This is not an email");
        } else {
            userEmail = tempEmail;
        }

        //Password should be more than 6 characters.
        if (inputPassword.length() < 5) {
            isPasswordValid = false;
            inputPassword.setError("Invalid Password");
        } else {
            isPasswordValid = true;
            userPassword = inputPassword.getText().toString();
        }
        if (isEmailValid && isPasswordValid) {
            isValid = true;
        }

        return isValid;

    }

    @OnClick(R.id.bt_signin)
    public void onSigninClicked() {
        if (validateInput()) {
            UserRepository userRepository = adapter.createRepository(UserRepository.class);
            userRepository.loginUser(userEmail, userPassword, new UserRepository.Callback() {
                @Override
                public void onSuccess(AccessToken token, UserModel currentUser) {
                    assignedToken = token.getId().toString();
                    firstname = currentUser.getFirstname();
                    lastname = currentUser.getLastname();
                    dob = currentUser.getDob();
                    ID = (String) currentUser.getId();

                    //// TODO: 25.05.17 Remove Log in onSigninClicked.
                    Log.i(TAG,"user token : "+assignedToken);
                    Log.i(TAG,"User firstname : "+firstname);
                    Log.i(TAG,"User lastname :"+lastname);
                    Log.i(TAG,"User date of birth :" +dob);
                    Log.i(TAG,"User ID :" +ID);

                    downloadUserInfoBackground();

                    Intent intentToHome =new Intent(LoginActivity.this,HomeActivity.class);
                    startActivity(intentToHome);

                }

                @Override
                public void onError(Throwable t) {
                    Log.e(TAG, "Login error", t);
                    //// TODO: 24.05.17 Notify error about the failed reason.
                }
            });
        }


    }

    /**
     * Grab the profile picture of the user from the server.
     */
    private void obtainProfilePic(){

    }

    @OnClick(R.id.link_register)
    public void onLinkRegisterClicked() {
        registerNewUser = true;
        Intent intentToRegisterPage = new Intent(this,RegisterUser.class);
        startActivity(intentToRegisterPage);

    }

    private void authenticationAnimation(){
        //// TODO: 24.05.17 show progress about authentication.
    }

    /**
     Important file which need to be saved.
     1. User token access.
     2. User first name.
     3. User last name.
     4. User email.
     5. User dob.
     6. User password.
     7. User ID.
     */
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"Login Activity ends");
        if(!registerNewUser){
            Log.i(TAG,"saving data");
            SharedPreferences userInfo = getSharedPreferences(USER_INFO,MODE_PRIVATE);
            SharedPreferences.Editor editor = userInfo.edit();
            editor.putString(Master.USERTOKEN,this.assignedToken);
            editor.putString(Master.FIRSTNAME,this.firstname);
            editor.putString(Master.LASTNAME,this.lastname);
            editor.putString(Master.EMAIL,this.userEmail);
            editor.putString(Master.DOB,this.dob);
            Log.i(TAG,"All date is saved in SharedPreferences");
        }
    }

    private void downloadUserInfoBackground(){
        //// TODO: 25.05.17 Do in Background to grap profile pic and score of the user
        Intent populateUserInfo = new Intent(getApplication(), ProfilePullService.class);
        populateUserInfo.setData(Uri.parse(BackgroundActivityCaller.DOWNLOAD_USERINFO));
        startService(populateUserInfo);
    }
}
