package com.udsoft.quizchamp.Models.Questions;

import com.strongloop.android.loopback.Model;

import java.util.ArrayList;

/**
 * Created by darwin on 25.05.17.
 */

public class QuestionModel extends Model{
    private  String question;
    private int level;
    private String answer;
    private ArrayList<String> choices;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public ArrayList<String> getChoices() {
        return choices;
    }

    public void setChoices(ArrayList<String> choices) {
        this.choices = choices;
    }
}
