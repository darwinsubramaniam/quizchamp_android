package com.udsoft.quizchamp.Models.Questions;

import com.strongloop.android.loopback.ModelRepository;

/**
 * Created by darwin on 25.05.17.
 */

public class QuestionRepository extends ModelRepository<QuestionModel> {
    public QuestionRepository() {
        super("question",QuestionModel.class);
    }
}
