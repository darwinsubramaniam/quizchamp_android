package com.udsoft.quizchamp.Models.User;

import com.strongloop.android.loopback.User;

/**
 * Created by darwin on 24.05.17.
 */

public class UserModel extends User {
    private String firstname;
    private String lastname;
    private String dob; //Format : DATE/MONTH/YEAR



    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }


    public class KEY{
        public final static String FIRSTNAME = "firstname";
        public final static String LASTNAME = "lastname";
        public final static String DOB = "dob";
    }
}


