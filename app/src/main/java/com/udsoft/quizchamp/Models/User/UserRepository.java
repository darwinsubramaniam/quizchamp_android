package com.udsoft.quizchamp.Models.User;


/**
 * Created by darwin on 24.05.17.
 */

public  class UserRepository extends com.strongloop.android.loopback.UserRepository<UserModel> {


    public interface Callback extends com.strongloop.android.loopback.UserRepository.LoginCallback<UserModel> {
    }

    public UserRepository(){
        super("user", null, UserModel.class);
    }
}
