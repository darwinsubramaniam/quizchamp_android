package com.udsoft.quizchamp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.strongloop.android.loopback.RestAdapter;
import com.strongloop.android.loopback.callbacks.VoidCallback;
import com.udsoft.quizchamp.Models.User.UserModel;
import com.udsoft.quizchamp.Models.User.UserRepository;
import com.udsoft.quizchamp.Server.Server;
import com.udsoft.quizchamp.UDSoft.Master;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.udsoft.quizchamp.UDSoft.Master.EMAIL;
import static com.udsoft.quizchamp.UDSoft.Master.PASSWORD;

public class RegisterUser extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private final static String TAG = "RegisterUser";
    @BindView(R.id.input_firstname)
    EditText inputFirstname;
    @BindView(R.id.input_lastname)
    EditText inputLastname;
    @BindView(R.id.show_user_dob)
    TextView showUserDob;
    @BindView(R.id.btn_set_dob)
    Button btnSetDob;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.input_retype_password)
    EditText inputRetypePassword;

    UserModel unRegisteredUserModel = new UserModel();
    UserModel registeredUser = new UserModel();
    boolean isDobSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_register);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.register_ok:
                if(createNewUser()){
                    Log.i(TAG, "Register new user to server");
                    //Check if the Phone is connected to mobile
                    ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
                    if(activeNetwork != null){
                        if(activeNetwork.getType() == connectivityManager.TYPE_WIFI || activeNetwork.getType() == connectivityManager.TYPE_MOBILE){
                            ProgressDialog loginProgress = new ProgressDialog(RegisterUser.this);
                            loginProgress.setMessage("Login to QuizChamp");
                            loginProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            loginProgress.setIndeterminate(true);
                            loginProgress.show();
                            registerNewUser();

                        }else{
                            Toast.makeText(RegisterUser.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(this,"No Internet Connection",Toast.LENGTH_LONG).show();
                    }

                }else
                {
                    Toast.makeText(this,"Failed to register as new user",Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    VoidCallback callback = new VoidCallback() {
        @Override
        public void onSuccess() {
            //Todo The user who succesfully registered should be logged in and never to return
            // to this page unless flag loggout is active in sharedpreferences.
            Log.i(TAG,"Created user in server");
            //TODO Create the layout for login in to the server with the user just created.
            Intent intentToAutoLogin = new Intent(RegisterUser.this,HomeActivity.class);
            intentToAutoLogin.putExtra(EMAIL,registeredUser.getEmail());
            intentToAutoLogin.putExtra(PASSWORD,registeredUser.getPassword());
            startActivity(intentToAutoLogin);
        }

        @Override
        public void onError(Throwable t) {
            Log.e(TAG,t.getMessage());
        }
    };

    private UserModel registerNewUser(){
        RestAdapter adapter = new Server().getLoopbackAdapter(this);
        UserRepository userRepository = adapter.createRepository(UserRepository.class);
        Map<String, String> userMap = new HashMap<>();
        userMap.put(UserModel.KEY.FIRSTNAME, unRegisteredUserModel.getFirstname());
        Log.i(TAG,"First name : " +unRegisteredUserModel.getFirstname());
        userMap.put(UserModel.KEY.LASTNAME, unRegisteredUserModel.getLastname());
        Log.i(TAG,"Last name : " +unRegisteredUserModel.getLastname());
        userMap.put(UserModel.KEY.DOB, unRegisteredUserModel.getDob());
        Log.i(TAG,"DOB : " + unRegisteredUserModel.getDob());
        UserModel userModel = userRepository.createUser(unRegisteredUserModel.getEmail(), unRegisteredUserModel.getPassword(),userMap);
        userModel.save(callback);
        Log.i(TAG,"Creating new user in server");
        return userModel;
    }

    private boolean createNewUser(){
        boolean isSuccess  = false;
        String firstName = inputFirstname.getText().toString();
        String lastName = inputLastname.getText().toString();
        String password =  inputPassword.getText().toString();
        String retypePassword = inputRetypePassword.getText().toString();
        String email = inputEmail.getText().toString();

        if(firstName.isEmpty()){
            inputFirstname.setError("First name is required");
            isSuccess = false;
        }else{
            unRegisteredUserModel.setFirstname(firstName);
            isSuccess = true;
        }

        if(lastName.isEmpty()){
            inputLastname.setError("Last name is required");
            isSuccess = false;
        }else{
            unRegisteredUserModel.setLastname(lastName);
            isSuccess = true;
        }

        if(password.equals(retypePassword)){
            if(password.length() < 5){
                Toast.makeText(this,"Password is too short",Toast.LENGTH_LONG);
                inputPassword.setError("Password is too short");
                isSuccess = false;
            }else{
                unRegisteredUserModel.setPassword(password);
                isSuccess = true;
            }
        }else {
            inputRetypePassword.setError("Retype did not match password");
            isSuccess = false;
        }

        if(Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            unRegisteredUserModel.setEmail(email);
            isSuccess = true;
        }else{
            inputEmail.setError("This is not an email");
            isSuccess = false;
        }

        return isSuccess;


    }


    @OnClick(R.id.btn_set_dob)
    public void onViewClicked() {
        Calendar calendar = Calendar.getInstance();
        int date = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog dateDialog = new DatePickerDialog(RegisterUser.this, (DatePickerDialog.OnDateSetListener) this,date,month,year);
        if(isDobSet){
            String dob = unRegisteredUserModel.getDob();
            String[] dobArray = dob.split("-");
            int date_dob = Integer.parseInt(dobArray[0]);
            int month_dob = Integer.parseInt(dobArray[1]);
            int year_dob = Integer.parseInt(dobArray[2]);
            Log.i(TAG, "DATE : " + date_dob +" MONTH : " + (month_dob +1) + " YEAR : " + year_dob);
            dateDialog.updateDate(year_dob,month_dob,date_dob);
        }else{
            int default_date_dob = 19;
            int default_month_dob = 8; // August as 0 is January
            int default_year_dob = 1990;
            dateDialog.updateDate(1990, 8,19);
        }
        dateDialog.show();
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        //Format is used by the backend server year-month-date
        String dob = year + "-" + (month+1) + "-" + dayOfMonth;
        Log.i(TAG,"onDateSet"+dob);
        unRegisteredUserModel.setDob(dob);
        //for showing the date of birth to user this format is used date/month/year.
        String forText = dayOfMonth +"/"+month+"/"+year;
        showUserDob.setText(forText);
        isDobSet = true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.register_menu,menu);
        return super.onPrepareOptionsMenu(menu);
    }
}
