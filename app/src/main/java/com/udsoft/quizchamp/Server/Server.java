package com.udsoft.quizchamp.Server;

import android.content.Context;

import com.strongloop.android.loopback.RestAdapter;

/**
 * Created by darwin on 24.05.17.
 */

public class Server {
    private RestAdapter adapter;

    // rest adapter to connect to server.
    public RestAdapter getLoopbackAdapter(Context context){
        if(adapter == null){
            adapter = new RestAdapter(context,
                    "http://192.168.1.189:3000/api");
        }
        return adapter;
    }
}
