package com.udsoft.quizchamp.Server.Services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import com.udsoft.quizchamp.UDSoft.*;
/**
 * Created by darwin on 25.05.17.
 */

public class ProfilePullService extends IntentService {

    private static final String TAG = "ProfilePullService";


    public ProfilePullService() {
        super("ProfilePullService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String dataString = intent.getDataString();
        Log.i(TAG,"Message recieved from LoginActivity is :" + dataString);
        switch (dataString){
            case  BackgroundActivityCaller.DOWNLOAD_USERINFO:
                downloadUserInfo();
                break;
            case BackgroundActivityCaller.UPDATE_USER_SCORE:
                UpdateUserScore();
                break;
            case BackgroundActivityCaller.UPDATE_USERINFO:
                updateUserInfo();
                break;
            default:

        }

    }

    private boolean updateUserInfo() {
        return false;
    }

    private boolean UpdateUserScore() {
        return false;
    }

    private boolean downloadUserInfo(){
        boolean isSuccess = false;
        Log.i(TAG,"Starting downloadUserInfo");
        Log.i(TAG,"Going to sleep for 10 second");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i(TAG,"Wake up from the 10 second sleep");
        return isSuccess;
    }
}
