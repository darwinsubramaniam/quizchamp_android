package com.udsoft.quizchamp.UDSoft;

/**
 * Created by darwin on 25.05.17.
 * This class contain all the backgroundActivity caller keys.
 * This class should be used to call all Quizchamp background running service.
 */



public final class BackgroundActivityCaller {

    /**
     * DOWNLOAD_USERINFOR is called to download all the details about
     * the user from the server.This might take some time so it is required
     * to run background so that the user experience will not be disturbed.
     */
    public static final String DOWNLOAD_USERINFO = "downloadUserInfo";

    /**
     * UPDATE_USERINFOR is the key for updating the user information to the
     * server.
     */
    public static final String UPDATE_USERINFO = "updateUserInfo";

    /**
     * UPDATE_SCORE is the key for updating the user score
     */
    public static final String UPDATE_USER_SCORE = "updateUserScore";


}
