package com.udsoft.quizchamp.UDSoft;

/**
 * Created by darwin on 25.05.17.
 */

/**
 * This  class keeps all the keys for SharedPreferences in Master.
 * Master is the sharedpreference which saves all the possible keys and value for default
 * Master is used to ensure the user experience in the app is pleasent.
 */
public final class Master {

    /**
     * USERTOKAN is the key for saving the token value for the logged in user.
     */
    public static final String USERTOKEN = "USERTOKEN";
    /**
     * DEFAULT_TOKEN is the default value when none user is logged in to the app yet.
     */
    public static final String DEFAULT_TOKEN = "Master_Token";
    /**
     * FIRSTNAME is the key for saving the first name of the user.
     */
    public static final String FIRSTNAME = "FIRSTNAME";

    /**
     * this is the default value of the first name for FIRSTNAME Key.
     */
    public static final String DEFAULT_FIRSTNAME ="Master";
    /**
     * LASTNAME is the key for saving the last name of the user.
     */
    public static final String LASTNAME = "LASTNAME";
    /**
     * this is the default value for the last name of the LASTNAME Key.
     */
    public static final String DEFAULT_LASTNAME ="QuizApp";
    /**
     * DOB is the key for saving the date of birth of the user
     */
    public static final String DOB = "DOB";
    /**
     * this is the default value for the DOB key
     */
    public static final String DEFAULT_DOB = "1990-08-19";
    /**
     * EMAIL is the key for saving email address of the user.
     */
    public static final String EMAIL = "EMAIL";
    /**
     *this is the default value for EMAIL key
     */
    public static final String DEFAULT_EMAIL = "quizchamp@unknown.com";
    /**
     * This key is used if intent passing password around.
     */
    public static final String PASSWORD = "PASSWORD";
    /**
     * This is th default password will be used to indicate error
     * qz is choosen because it is less than 5 for possibility for user
     * to have this password is not possible even the server require more than 5 password.
     */
    public static final String DEFAULT_PASSWORD ="qz";
    /**
     * Condition where new register user is create. true if new user is created succesfully.
     */
    public static final String REGISTERED_NEW_USER ="newUser";
    //TODO Darwin Complete the way each pages will interect with each other .

}
